package nested_classes;

/**
 * ## Zadanie 2
 * ### Klasa Movie
 * Zaimplementuj klasę `Movie`, która będzie zawierać pola reprezentujące informacje takie jak:
 * tytuł, reżyser, rok wydania, gatunek, wydawca. Klasa ta powinna zawierać domyślny konstruktor
 * oraz metody typu `getter` i `setter`, oraz nadpisaną metdoę `toString`, która będzie
 * odpowiedzialna za zwracanie informacji o właściwościach konkretnego filmu.
 * <p>
 * ### Klasa MovieCreator
 * Zaimplementuj klasę zagnieżdżoną statyczną `MovieCreator`. Klasa ta powinna:
 * * zawierać pola klasy takie same jak klasa Movie
 * * zawierać metody umoliżwiające ustawianie konkrentych właściwości filmu. Każda z metod powinna
 * * zwracać instancję obiektu, na rzecz którego wywoływana jest metoda
 * * metodę `createMovie`, która na podstawie ustawionych parametrów stworzy
 * * instancję klasy `Movie` i zwróci ją w rezultacie działania metody
 */

enum Genre {
    ACTION, ADVENTURE, ANIMATION, COMEDY, DRAMA, HORROR, THRILLER, ROMANCE;
}

public class Movie {

    private String title;
    private String producer;
    private int yearOfRelease;
    private Genre genre;
    private String publisher;

    public Movie(String title, String producer, int yearOfRelease, Genre genre, String publisher) {
        this.title = title;
        this.producer = producer;
        this.yearOfRelease = yearOfRelease;
        this.genre = genre;
        this.publisher = publisher;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return String.format(
                "\n Film title: %s, \n Film producer: %s, \n Year of release: %d, \n Genre: %s, \n Publisher: %s",
                title, producer, yearOfRelease, genre, publisher);
    }

    // Zaimplementuj klasę zagnieżdżoną statyczną `MovieCreator`. Klasa ta powinna:
    static class MovieCreator {

        // 1. zawierać pola klasy takie same jak klasa Movie
        private String title;
        private String producer;
        private int yearOfRelease;
        private Genre genre;
        private String publisher;

        // 2. zawierać metody umoliżwiające ustawianie konkrentych właściwości filmu.
        // Każda z metod powinna zwracać instancję obiektu, na rzecz którego wywoływana jest metoda
        public MovieCreator setMovieTitle(MovieCreator movieCreator, String title) {
            movieCreator.title = title;
            return movieCreator;
        }

        public MovieCreator setMovieProducer(MovieCreator movieCreator, String producer) {
            movieCreator.producer = producer;
            return movieCreator;
        }

        public MovieCreator setMovieYearOfRelease(MovieCreator movieCreator, int yearOfRelease) {
            movieCreator.yearOfRelease = yearOfRelease;
            return movieCreator;
        }

        public MovieCreator setMovieGenre(MovieCreator movieCreator, Genre genre) {
            movieCreator.genre = genre;
            return movieCreator;
        }

        public MovieCreator setMoviePublisher(MovieCreator movieCreator, String publisher) {
            movieCreator.publisher = publisher;
            return movieCreator;
        }

        // 3. metodę `createMovie`, która na podstawie ustawionych parametrów
        // stworzy instancję klasy `Movie` i zwróci ją w rezultacie działania metody
        public Movie createMovie(MovieCreator movieCreator) {
            return new Movie(
                    movieCreator.title,
                    movieCreator.producer,
                    movieCreator.yearOfRelease,
                    movieCreator.genre,
                    movieCreator.publisher
            );
        }
    }

    public static void main(String[] args) {
        MovieCreator movieCreator = new MovieCreator();
        movieCreator.setMovieTitle(movieCreator, "Home Alone");
        movieCreator.setMovieProducer(movieCreator, "John Hughes");
        movieCreator.setMovieYearOfRelease(movieCreator, 1990);
        movieCreator.setMovieGenre(movieCreator, Genre.COMEDY);
        movieCreator.setMoviePublisher(movieCreator, "20th Century Fox");
        Movie movie = movieCreator.createMovie(movieCreator);
        System.out.println(movie);
    }
}
