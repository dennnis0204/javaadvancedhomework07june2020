package nested_classes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Zaimplementuj klasę `UserValidator`, która w ramach metody `validateEmails` będzie odpowiedzialna
 * za walidację danych użytkownika: email, email alternatywny. W ramach metody `validateEmails` zadeklaruj
 * klasę lokalną `Email`, która będzie odpowiedzialna za formatowanie wskazanego adresu email uwzględniając następujące reguły:
 * jeśli wskazany adres email jest pusty, bądź jest nullem należy ustawić mu wartość `unknown`
 * jeśli wskazany adres email nie spełnia założeń adresu email, należy ustawić wartość `unknown` (skorzystaj w tym celu z wyrażeń regularnych)
 */

public class UserValidator {

    private static final String UNKNOWN = "unknown";
    static String regularExpression = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:" +
            "[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*" +
            "\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4]" +
            "[0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0" +
            "-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(regularExpression, Pattern.CASE_INSENSITIVE);


    public static void validateEmails(String email, String alternativeEmail) {

        class Email {
            String validatedEmail = null;

            Email(String email) {
                if (email == null || email.equals("")) {
                    validatedEmail = UNKNOWN;
                } else if (!validateEmailWithRegex(email)) {
                    validatedEmail = UNKNOWN;
                } else {
                    validatedEmail = email;
                }
            }

            public String getEmail() {
                return validatedEmail;
            }

            public boolean validateEmailWithRegex(String emailStr) {
                Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
                return matcher.find();
            }
        }

        Email theEmail = new Email(email);
        Email theAlternativeEmail = new Email(alternativeEmail);

        if (theEmail.getEmail().equals(UNKNOWN)) {
            System.out.println(String.format("Main email %s is invalid!!!", email));
        } else {
            System.out.println(String.format("Main email %s is valid", email));
        }
        if (theAlternativeEmail.getEmail().equals(UNKNOWN)) {
            System.out.println(String.format("Alternative email %s is invalid!!!", alternativeEmail));
        } else {
            System.out.println(String.format("Alternative email %s is valid", alternativeEmail));
        }
    }

    public static void main(String[] args) {
        validateEmails("john.doe@gmail.com", "d@com");
    }
}
